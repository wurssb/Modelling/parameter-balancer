"""Implementation of Lubitz' parameter balancing as outlined in the paper below.

Lubitz, T., Schulz, M., Klipp, E., & Liebermeister, W. (2010).
    Parameter balancing in kinetic models of cell metabolism.
    The Journal of Physical Chemistry. B, 114(49), 16298-303.
    http://doi.org/10.1021/jp108764b

Author: Rik van Rosmalen
"""
import setuptools


def readme():
    """Load readme from README.md."""
    with open("README.md", "r") as fh:
        return fh.read()


setuptools.setup(
    name="parameter_balancer",
    version="0.0.1",
    author="Rik van Rosmalen",
    author_email="rikpetervanrosmalen@gmail.com",
    description="Implementation of Lubitz et al. parameter balancing algorithm "
                "used for creating consistent metabolic network parameterizations.",
    long_description=readme(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/wurssb/Modelling/parameter-balancer",
    packages=setuptools.find_packages(),
    classifiers=(
        # "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
    install_requires=(
        'pandas',
        'numpy',
        'scipy',
        'symbolicSBML'
    ),
    dependency_links=[
        'git+https://gitlab.com/wurssb/Modelling/symbolicsbml.git@master#egg=symbolicSBML-0'
    ],
    extras_require={
        'lhs': ["pyDOE"]
    }
)
