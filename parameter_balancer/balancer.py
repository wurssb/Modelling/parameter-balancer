"""Implementation of Lubitz' parameter balancing as outlined in the paper below.

Lubitz, T., Schulz, M., Klipp, E., & Liebermeister, W. (2010).
    Parameter balancing in kinetic models of cell metabolism.
    The Journal of Physical Chemistry. B, 114(49), 16298-303.
    http://doi.org/10.1021/jp108764b

TODO:
    - Helper function to convert between weg / cat / hal parametrisations
    - Test case to show that weg parametrisation always converges (or in symbolicSBML?)
    - Include new parameters
    - Automated mapping to combined parameters (vmax, km_tot_rate etc.)

Author: Rik van Rosmalen
"""
from __future__ import division, print_function

import pandas as pd

import numpy as np

import scipy.linalg
import scipy.sparse
import scipy.stats

from symbolicSBML import SBMLModel, Parameters

# Previously, None or NaN was used but this works awfully with the indices of Dataframes.
NA_IDENTIFIER = ''


def to_linear_arithmetic_mean(logx, logsd, target=None):
    """Convert the values and standard deviation of x to standard linear scale.

    Note: Energies quantities should not be converted to log scale. A target array
    can be used where target is false for non-log scalable values.
    Note: This calculates the arithmetic mean and variance of a logarithmic distribution.

    :param logx: Mean of the distribution in log scale.
    :type logx: array[float]
    :param logsd: Standard deviation of the distribution in log scale.
    :type logsd: array[float]
    :param target: target array, where 1 means that the value should be converted and 0
    that it should be left alone. If none (default), all values will be converted.
    :type target: array[bool]

    :returns: Arithmetic Mean and standard deviation in normal scale.
    :rtype: (array[float], array[float])
    """
    # To prevent overflow errors we don't care about, set energy to 1 (in a copy!)
    mlogx = np.array(logx)
    mlogsd = np.array(logsd)
    if target is None:
        target = np.ones(mlogx.shape, dtype=bool)
    else:
        mlogx[np.logical_not(target)] = 1
        mlogsd[np.logical_not(target)] = 1
    # Calculate arithmetic mean and s.d.
    x = np.exp(mlogx + 0.5 * np.square(mlogsd))
    var = np.expm1(np.square(mlogsd)) * np.exp(2 * mlogx + np.square(mlogsd))
    sd = np.sqrt(var)
    # Only return the new values where target is True
    return np.where(target, x, logx), np.where(target, sd, logsd)


def to_linear_geometric_mean(logx, target=None):
    """Convert the values  of x to standard linear scale.

    Note: Energies quantities should not be converted to log scale.
    A masking array target can be used where target is true for non-energy values.
    Note: This calculates the geometric mean of a logarithmic distribution.

    :param logx: Mean of the distribution in log scale.
    :type logx: array[float]
    :param target: target array, where 1 means that the value should be converted and 0
    that it should be left alone. If none (default), all values will be converted.
    :type target: array[bool]

    :returns: Geometric Mean (median of log-normal distribution) in normal scale.
    :rtype: array[float]
    """
    # To prevent overflow errors we don't care about, set energy to 1 (in a copy!)
    mlogx = np.array(logx)
    if target is None:
        target = np.ones(mlogx.shape, dtype=bool)
    else:
        mlogx[np.logical_not(target)] = 1
    # Calculate arithmetic mean
    x = np.exp(mlogx)

    # Only return the new values where target is True
    return np.where(target, x, logx)


def to_log_mean(mu, sigma, mask=None):
    """Convert the log normal distributions (mu, sigma) to log scale.

    This calculates the arithmetic mean and variance of a logarithmic distribution.

    :param mu: mean (in linear scaling)
    :type mu: array[N] float
    :param sd: standard deviation (in linear scaling)
    :type sd: array[N] float
    :param mask: Boolean mask for values that should not be scaled, defaults to None
    :type mask: array[N] bool or None, optional

    :returns: mean, standard deviation - in log scaling
    :rtype: array[N] float, array[N] float
    """
    # To prevent overflow errors we don't care about, set energy to 1 (in a copy!)
    mx = np.array(mu)
    msd = np.array(sigma)
    if mask is None:
        mask = np.zeros(mx.shape, dtype=bool)
    else:
        mx[mask] = 1
        msd[mask] = 1
    # Calculate log normal values
    logx = np.log(mx) - 0.5 * np.log1p(np.square(msd / mx))
    logvar = np.log1p(np.square(msd / mx))
    logsd = np.sqrt(logvar)
    # Only return the new values where mask is False
    return (np.where(mask, mu, logx),
            np.where(mask, sigma, logsd))


class ParameterData(object):
    _dtypes = {"parameter_type": 'O', "metabolite": 'O', "reaction": 'O',
               "mean": float, "sd": float}

    def __init__(self, parameter_type, metabolite, reaction, mean, sd):
        """Create a parameter data object.

        Data object to group parameter data. Each argument should be a list/array of values,
        where each entry corresponds to a single parameter data point.

        If the the reaction or metabolite is not relevant, use a None value to signify this.
        A log-normal distribution is assumed for non-energy values, but values should be given in
        natural (i.e. non-logarithmic) scale.

        Note that a standard deviation of 0 cannot be used in parameter balancing, use a small
        but positive uncertainty range instead.

        Alternatively, the data object can be created from a DataFrame.

        :param parameter_type: Parameter type
        :type parameter_type: list[str, ...]
        :param reaction: Reaction name
        :type reaction: list[str, ...]
        :param metabolite: Metabolite name
        :type metabolite: list[str, ...]
        :param mean: Mean of parameter value estimate
        :type mean: list[float, ...]
        :param sd: Standard deviation of parameter value estimate
        :type sd: list[float, ...]

        :raises ValueError: A valueError is raised on data inconsistencies.
        """
        # Replace nan for consistency.
        reaction = [i if not pd.isna(i) else NA_IDENTIFIER for i in reaction]
        metabolite = [i if not pd.isna(i) else NA_IDENTIFIER for i in metabolite]
        self.df = pd.DataFrame(data=list(zip(parameter_type, metabolite, reaction,
                                             mean, sd)),
                               columns=["parameter_type", "metabolite", "reaction",
                                        "mean", "sd"]).astype(self._dtypes)
        self.df.index.name = 'entry'
        self.verify()

    @classmethod
    def empty(cls):
        """Alternative constructor for an empty ParameterData object."""
        return cls([], [], [], [], [])

    @classmethod
    def from_dataframe(cls, parameter_dataframe):
        """Alternative constructor utilizing a Pandas DataFrame.

        The DataFrame should have the same columns as the arguments of the
        main initialization function.

        :param parameter_dataframe: Parameter data
        :type parameter_dataframe: pandas.DataFrame

        :raises ValueError: A ValueError is raised on data inconsistencies.
        """
        df = parameter_dataframe
        # Check for right columns
        required_columns = set(cls._dtypes.keys())
        if set(df.columns) != required_columns:
            raise ValueError("Incorrect data columns.")

        return cls(df["parameter_type"], df["metabolite"], df["reaction"],
                   df["mean"], df["sd"])

    def verify(self):
        """Verify the internal consistency of the data object.

        This function is automatically called upon construction. Note that
        errors are checked one-by-one so multiple rounds of fixing and checking
        might be required.

        :raises ValueError: A ValueError is raised on data inconsistencies.
        """
        # An empty DataFrame fulfils all criteria.
        if self.df.empty:
            return

        # Check for NaN/inf values in mean or sd
        if not np.all(np.isfinite(self.df[['mean', 'sd']])):
            rows = np.unique(np.where(~np.isfinite(self.df[['mean', 'sd']]))[0])
            raise ValueError("Infinite or NaN value in mean or sd columns: \n{}"
                             .format(self.df.iloc[rows]))

        # Check for 0 or negative values in sd
        if np.any(self.df[['sd']] <= 0):
            rows, _ = np.where(self.df[['sd']] <= 0)
            raise ValueError("Cannot have values <= 0 for sd: \n{}"
                             .format(self.df.iloc[rows]))

        # Check for 0 or negative values in mean for log values.
        if np.any(self.df[['mean']] <= 0):
            n_rows, _ = np.where(self.df[['mean']] <= 0)
            if np.any(self.df.loc[n_rows, 'parameter_type'].isin(Parameters.log)):
                e_rows = self.df.loc[n_rows].loc[self.df.loc[n_rows, 'parameter_type']
                                                 .isin(Parameters.log)]
                raise ValueError("Cannot have values <= 0 for mean of log distributed parameter:"
                                 " \n{}".format(e_rows))

        # Check if all parameter types are defined ones.
        if not set(self.df['parameter_type']) <= Parameters.all_balanced_parameters:
            parameter = self.df['parameter_type'] - Parameters.all_balanced_parameters
            rows = self.df[self.df['parameter_type'].isin(parameter)]
            raise ValueError("Invalid parameter type found: \n{}".format(rows))

        # Check if parameters have the right combo of metabolites
        # and reactions depending on the type.
        should_have_metabolite = (self.df['parameter_type']
                                  .isin(Parameters.specific_to_metabolite))
        has_metabolite = self.df['metabolite'].str.len() > 0

        if np.any(should_have_metabolite != has_metabolite):
            idx = np.nonzero(should_have_metabolite != has_metabolite)[0]
            raise ValueError("Parameters have missing/superfluous metabolite identifier: \n{}"
                             .format(self.df.iloc[idx]))

        should_have_reaction = (self.df['parameter_type']
                                .isin(Parameters.specific_to_reaction))
        has_reaction = self.df['reaction'].str.len() > 0

        if np.any(should_have_reaction != has_reaction):
            idx = np.nonzero(should_have_metabolite != has_metabolite)[0]
            raise ValueError("Parameter have missing/superfluous reaction identifier: \n{}"
                             .format(self.df.iloc[idx]))

    @property
    def columns(self):
        """Return the columns or type of each parameter entry as an iterator.

        Note: This format is the same as the base and derived quantities in the PB class.
        :returns: iterator of (parameter_type, metabolite, reaction)
        :rtype: iterator[tuple(parameter_type, metabolite, reaction)]
        """
        return self.df[['parameter_type', 'metabolite', 'reaction']].itertuples(index=False,
                                                                                name=None)

    def __repr__(self):
        return repr(self.df)


class BalancedParameterData(object):
    def __init__(self, q_post, C_post, Q, columns, T, R):
        """Parameter Balancing result object.

        Object that captures the result of the parameter balancing algorithm.
        Allows the calculation of distribution properties or sampling.
        Can be saved to disk and loaded without loss of information.

        :param q_post: log-mean of the base quantities.
        :type q_post: array[float]
        :param C_post: log-covariance of the base quantities.
        :type C_post: array[float n_base_quantities x n_base_quantities]
        :param Q: Forward prediction matrix, used to calculate the derived quantities.
        :type Q:  array[Float, (n_base_quantities + n_derived_quantities) x n_base_quantities]
        :param columns: columns of the base and derived quantities in the same order as in Q.
        :type columns: list of (parameter_type, metabolite, reaction)
        """
        self.q_post = q_post
        self.C_post = C_post
        self.Q = Q
        self.columns = columns
        self.T = T
        self.R = R

        self.nonlog = np.array([i in Parameters.log for i, _, _ in columns], dtype=bool)

        self._median = None
        self._mean = None
        self._sd = None

    def to_frame(self, median=True, mean=False, sd=True, quantiles=False, noRT=False):
        """Return the result object as a pandas DataFrame with mean/median/std.

        :param median: Add median to the resulting DataFrame or not.
        :type median: bool
        :param mean: Add mean to the resulting DataFrame or not.
        :type mean: bool
        :param sd: Add sd to the resulting DataFrame or not.
        :type sd: bool
        :param quantiles: Add quantiles to the resulting DataFrame or not.
        :type quantiles: bool

        :returns: Pandas DataFrame with the requested values.
        :rtype: pd.DataFrame
        """
        columns = pd.MultiIndex.from_tuples(self.columns)
        data = []
        index = []
        T_val, R_val = [], []
        if median:
            data.append(self.median)
            index.append('median')
            T_val.append(self.T)
            R_val.append(self.R)
        if mean:
            data.append(self.mean)
            index.append('mean')
            T_val.append(self.T)
            R_val.append(self.R)
        if sd:
            data.append(self.sd)
            index.append('sd')
            T_val.append(0)
            R_val.append(0)
        if quantiles:
            raise NotImplementedError("Quantiles yet to be implemented.")

        df = pd.DataFrame(columns=columns, data=data, index=index)
        if not noRT:
            df[(Parameters.T, NA_IDENTIFIER, NA_IDENTIFIER)] = T_val
            df[(Parameters.R, NA_IDENTIFIER, NA_IDENTIFIER)] = R_val
        return df.sort_index(axis=1)

    def _samples_to_log(self, samples, only_base):
        """Convert samples from the probability distribution to the appropriate scale.

        If only base is False, also calculate the derived parameters.
        """
        # TODO: Possibilities for improved vectorization here?
        if not only_base:
            return np.vstack([to_linear_geometric_mean(self.Q.dot(i), self.nonlog)
                              for i in samples])
        else:
            return np.vstack([to_linear_geometric_mean(i, self.nonlog[:len(self.q_post)])
                              for i in samples])

    def _samples_to_df(self, samples, only_base, noRT):
        """From a array of samples, create a DataFrame with all parameter names etc."""
        # Get names for all parameters or only the base parameters.
        if only_base:
            columns = pd.MultiIndex.from_tuples(self.columns[:len(self.q_post)])
        else:
            columns = pd.MultiIndex.from_tuples(self.columns)

        # Create the DataFrame.
        df = pd.DataFrame(columns=columns, data=samples)

        # If wanted, we add the final 2 system parameters as well.
        if not noRT:
            df[(Parameters.T, NA_IDENTIFIER, NA_IDENTIFIER)] = self.T
            df[(Parameters.R, NA_IDENTIFIER, NA_IDENTIFIER)] = self.R

        # Update index name and sort.
        df.index.name = 'sample'
        return df.sort_index(axis=1)

    def sample(self, n=1, only_base=False, noRT=False):
        """Return a pandas DataFrame with samples using a (log) normal error distribution.

        :param n: Amount of samples, defaults to 1
        :type n: int
        :param only_base: Return only the base quantities or also the derived quantities,
        defaults to False
        :type only_base: bool

        :returns: Pandas DataFrame with the requested samples as rows.
        :rtype: pd.DataFrame
        """
        samples = np.random.multivariate_normal(self.q_post, self.C_post, n)
        # Map to log where appropriate and calculate derived quantities
        samples = self._samples_to_log(samples, only_base)

        return self._samples_to_df(samples, only_base, noRT)

    def sample_lhs(self, n=1, only_base=False, noRT=False, **lhs_args):
        """Return a pandas DataFrame sampled using a latin hypercube.

        :param n: Amount of samples, defaults to 1
        :type n: int
        :param only_base: Return only the base quantities or also the derived quantities,
        defaults to False
        :type only_base: bool
        :param lhs_args: Arguments to be passed down to the lhs function.
        For more documentation see `pyDOE.lhs`

        :returns: Pandas DataFrame with the requested samples as rows.
        :rtype: pd.DataFrame
        """
        # Create latin hypercube samples using pyDOE
        try:
            import pyDOE
        except ImportError:
            print("Please install pyDOE for lhs design (`pip install pyDOE`).")
            raise
        lhd = pyDOE.lhs(len(self.q_post), n, **lhs_args)

        # Map to multivariate normal distribution
        # uses the same implementation as np.random.multivariate_normal
        lhd_norm = scipy.stats.norm.ppf(lhd)
        _, s, v = scipy.linalg.svd(self.C_post)
        samples = self.q_post + lhd_norm.dot(np.sqrt(s)[:, np.newaxis] * v)

        # Implementation using Cholesky decomposition
        # See: https://stackoverflow.com/a/16025584
        # L = np.linalg.cholesky(self.C_post)
        # lhd_norm = scipy.stats.norm.ppf(lhd)
        # samples = (L.dot(lhd_norm.T) + self.q_post[:, np.newaxis]).T

        # TODO: Which is preferred? Cholesky seems faster?
        # Numpy docs mention that Choleksy is not used to preserve current outputs.
        # Does that mean it's just for backward compatibility?
        # Previous versions mention that Cholesky would be better.

        # Alternative naive implementation
        # Note: This does not consider covariance. (it is wrong!)
        # std = np.sqrt(self.C_post.diagonal())
        # samples = np.array(lhd)
        # for i in range(len(self.q_post)):
        #     samples[:, i] = scipy.stats.norm(loc=self.q_post[i],
        #                                      scale=std[i]).ppf(lhd[:, i])

        # Map to log where appropriate and calculate derived quantities
        log_samples = self._samples_to_log(samples, only_base)

        # Create and return the DataFrame
        x = self._samples_to_df(log_samples, only_base, noRT)
        return x

    def save(self, path):
        """Save the results to a compressed numpy archive.

        The results can be loaded by :class:BalancedParameterData.load(path).

        :param path: File path
        :type path: str
        """
        columns = np.array(self.columns, dtype='U')
        arrays = {'q_post': self.q_post,
                  'C_post': self.C_post,
                  'Q': self.Q,
                  'columns': columns,
                  'T': np.array([self.T]),
                  'R': np.array([self.R])}
        np.savez(path, **arrays)

    @classmethod
    def load(cls, path):
        """Load the results from a compressed numpy archive.

        The results can be saved by :class:BalancedParameterData.save(path).

        :param path: File path
        :type path: str
        """
        x = np.load(path)
        T = x.f.T[0]
        R = x.f.R[0]
        columns = [tuple(j if j else NA_IDENTIFIER for j in i) for i in x.f.columns]
        return cls(x.f.q_post, x.f.C_post,
                   x.f.Q, columns,
                   T, R)

    @property
    def median(self):
        """Calculate the median of all distributions."""
        if self._median is None:
            self._median = to_linear_geometric_mean(self.Q.dot(self.q_post),
                                                    target=self.nonlog)
        return self._median

    @property
    def mean(self):
        """Calculate the mean of all distributions."""
        if self._mean is None:
            C_x_post = self.Q.dot(self.C_post).dot(self.Q.T)
            sigma = np.sqrt(np.diag(C_x_post))
            self._mean, self._sd = to_linear_arithmetic_mean(self.Q.dot(self.q_post),
                                                             sigma, target=self.nonlog)
        return self._mean

    @property
    def sd(self):
        """Calculate the standard deviation of all distributions."""
        if self._sd is None:
            # Calculating the mean property will also calculate the sd property.
            self.mean
        return self._sd

    def __repr__(self):
        return repr(self.to_frame())


class Balancer(object):
    def __init__(self, priors, data, structure, T=300., R=8.314 / 1000., augment=True,
                 cooperativities=None):
        """Create a balancing object.

        Create a balancing object given the priors, data, structure and others settings.

        :param priors: Prior distributions for basic quantities.
        :type priors: dictionary
        :param data: Dataset of previously measured parameters and their uncertainty.
        :type data: :class:ParameterData
        :param structure: Model structure
        :type structure: :class:SBMLModel
        :param T: Temperature (in Kelvin), defaults to 300K (=26.85 degrees C).
        :type T: float
        :param R: Gas constant (in J/mmol/K), defaults to 8.314/1000.
        :type R: float
        :param augment: Whether to augment the data with priors for dependent quantities
        or not. Can also be further specified to 'missing' for only augmenting data for
        parameters that are not in the data, or 'full' for augment the data with priors for
        all parameters, regardless of their existence in the data. Defaults to only missing.
        :type augment: bool, optional OR 'missing' or 'full'
        :param cooperativities: Dictionairy of optional cooperativity coefficients per reaction.
        Defaults to 1 for missing keys. (i.e. keep the stoichiometry as is.)
        :type cooperativities: dictionairy[reaction: cooperativity index], optional
        """
        self.priors = priors
        self.data = data
        self.structure = structure
        self.R = R
        self.T = T
        self.dependencies = Parameters.dependencies(self.R, self.T)
        if augment in (True, 1, 'missing'):
            self.augment = True
            self.augment_only_missing = True
        elif augment == 'full':
            self.augment = True
            self.augment_only_missing = False
        else:
            self.augment = False
            self.augment_only_missing = True
        self.cooperativities = dict(cooperativities) if cooperativities is not None else {}

        self.verify_consistency()

        self.base_quantities = self.get_base_quantities()
        self.derived_quantities = self.get_derived_quantities()
        self.all_quantities = self.base_quantities + self.derived_quantities

    def verify_consistency(self):
        """Verify the consistency of the data with the network structure.

        Verify that all reactions and metabolites of the parameters in the data
        are consistent with the names used in the structure object.

        :raises ValueError: A ValueError is raised if there is a name in the data not defined
        in the structure.
        """
        # Check if all reactions match
        if not set(self.data.df['reaction']) <= (set(self.structure.reactions) |
                                                 {NA_IDENTIFIER}):
            reactions = (set(self.data.df['reaction']) -
                         set(self.structure.reactions) -
                         {NA_IDENTIFIER})
            rows = self.data.df[self.data.df['reaction'].isin(reactions)]
            raise ValueError("Reaction name in data not found in structure. \n{}"
                             .format(rows))

        # Check if all metabolites match
        if not set(self.data.df['metabolite']) <= (set(self.structure.metabolites) |
                                                   {NA_IDENTIFIER}):
            metabolites = (set(self.data.df['metabolite']) -
                           set(self.structure.metabolites) -
                           {NA_IDENTIFIER})
            rows = self.data.df[self.data.df['reaction'].isin(metabolites)]
            raise ValueError("Metabolite name in data not found in structure. \n{}"
                             .format(rows))

        # Check if all parameters agree with the stoichiometry.
        # This is only necessary for parameters both specific to reactions and metabolites.
        for p_type, metabolite, reaction in self.data.columns:
            if p_type in Parameters.specific_to_metabolite & Parameters.specific_to_reaction:
                row = self.structure.metabolite_index[metabolite]
                column = self.structure.reaction_index[reaction]
                if self.structure.stoichiometry[row, column] == 0:
                    raise ValueError("Invalid parameter '{}' for combination of '{}', '{}': "
                                     "Combination not in stoichiometry matrix."
                                     .format(p_type, metabolite, reaction))

        # Check if all reactions in the cooperativities dictionairy exist.
        if not set(self.cooperativities.keys()) <= set(self.structure.reactions):
            reactions = set(self.cooperativities.keys()) - set(self.structure.reactions)
            raise ValueError("Reaction name in cooperativities not found in strucure. \n{}"
                             .format(reactions))

    def get_base_quantities(self):
        """Return the order of the base quantities in a list.

        :returns: base quantities
        :rtype: list of (parameter_type, metabolite, reaction)
        """
        # Construct the row layout (base quantities):
        columns = []  # list of (base quantity, compound, reaction)
        # For each reaction: enzyme concentration (u), velocity constant(kV)
        columns.extend([(Parameters.u, NA_IDENTIFIER, r) for r in self.structure.reactions])
        columns.extend([(Parameters.kv, NA_IDENTIFIER, r) for r in self.structure.reactions])
        # For each reaction & metabolite pair: Michaelis constant (kM)
        for i, j in zip(*np.where(self.structure.stoichiometry)):
            columns.append((Parameters.km,
                            self.structure.metabolites[i],
                            self.structure.reactions[j]))
        # For each compound: concentration (c), chemical potential (mu)
        columns.extend([(Parameters.c, s, NA_IDENTIFIER) for s in self.structure.metabolites])
        columns.extend([(Parameters.mu, s, NA_IDENTIFIER) for s in self.structure.metabolites])
        return columns

    def get_derived_quantities(self):
        """Return the order of the derived quantities in a list.

        :returns: derived quantities
        :rtype: list of (parameter_type, metabolite, reaction)
        """
        # Construct the row layout
        # (derived quantities, these should be behind the base quantities).
        columns = []  # list of (derived quantity, compound, reaction)
        # For each reaction:
        columns.extend([(Parameters.keq, NA_IDENTIFIER, r) for r in self.structure.reactions])
        columns.extend([(Parameters.kcat_prod, NA_IDENTIFIER, r) for r in self.structure.reactions])
        columns.extend([(Parameters.kcat_sub, NA_IDENTIFIER, r) for r in self.structure.reactions])
        columns.extend([(Parameters.vmax, NA_IDENTIFIER, r) for r in self.structure.reactions])
        columns.extend([(Parameters.A, NA_IDENTIFIER, r) for r in self.structure.reactions])
        # For each compound:
        columns.extend([(Parameters.mu_p, s, NA_IDENTIFIER) for s in self.structure.metabolites])
        return columns

    def augment_data(self, only_missing=True):
        """Add priors for the derived quantities to the data as pseudo-priors.

        :returns: A new ParameterData object (or the old one if nothing was added.)
        :rtype: :class:ParameterData
        """
        augment = []
        excisting = set(self.data.columns)
        for quantity in self.derived_quantities:
            # Skip existing if required
            if only_missing and quantity in excisting:
                continue
            augment.append(quantity + tuple(self.priors[quantity[0]]))

        # No pseudo priors added, so we can just return the original data.
        if not augment:
            return self.data

        original_data = self.data.df.copy()

        # Create new DataFrame
        augmented_data = pd.DataFrame(augment)
        # Make sure columns match
        augmented_data.columns = original_data.columns

        # Combine and drop index so there's no duplicate entries.
        combined = pd.concat((original_data, augmented_data), axis=0).reset_index(drop=True)
        combined.index.name = 'entry'

        return ParameterData.from_dataframe(combined)

    def build_priors(self):
        """Create the prior information based on the reaction structure.

        :returns: (prior mean, prior standard deviation)
        :rtype: (array[Float], array[Float])
        """
        q_prior = np.zeros(len(self.base_quantities))
        sigma_prior = np.zeros(len(self.base_quantities))
        for i, (parameter_type, _, _) in enumerate(self.base_quantities):
            mu, sigma = self.priors[parameter_type]
            q_prior[i] = mu
            sigma_prior[i] = sigma

        return q_prior, sigma_prior

    def forward_dependency_matrix(self, sparse=False, annotated=False):
        """Create the forward dependency matrix depending on the structure.

        The forward dependency matrix or prediction matrix predicts the complete
        set of derived quantities (rows) from the set of base quantities (columns).

        :param sparse: Return a sparse matrix instead of a dense one, defaults to False.
        :type sparse: bool
        :param annotated: Return a Pandas DataFrame instead of a numpy matrix with annotated
        rows and columns.
        :type annotated: bool

        :returns: Forward dependency matrix
        :rtype: array[Float, (n_base_quantities + n_derived_quantities) x n_base_quantities]
        """
        columns = self.base_quantities
        rows = self.all_quantities
        return self._dependency_matrix(rows, columns, sparse, annotated)

    def backward_dependency_matrix(self, sparse=False, annotated=False):
        """Create the backward dependency matrix depending on the structure.

        The backward dependency matrix or data dependence matrix maps the set of parameters (rows)
        to the set of base quantities and is used to estimate the base parameters (columns).

        :param sparse: Return a sparse matrix instead of a dense one, defaults to False.
        :type sparse: bool
        :param annotated: Return a Pandas DataFrame instead of a numpy matrix with annotated
        rows and columns.
        :type annotated: bool

        :returns: Backward dependency matrix
        :rtype: array[Float, n_data_parameters x n_base_quantities]
        """
        columns = self.base_quantities
        rows = list(self.data.columns)
        return self._dependency_matrix(rows, columns, sparse, annotated)

    def _dependency_matrix(self, rows, columns, sparse=False, annotated=False):
        """Create a dependency matrix from the rows and columns according to the dependencies.

        :param rows: Parameters on the first axis (rows).
        :type rows: list of (parameter_type, metabolite, reaction)
        :param columns: Parameters on the second axis (rows).
        :type columns: list of (parameter_type, metabolite, reaction)
        :param sparse: Return a sparse matrix instead of a dense one, defaults to False.
        :type sparse: bool
        :param annotated: Return a Pandas DataFrame instead of a numpy matrix with annotated
        rows and columns.
        :type annotated: bool

        :returns: Dependency matrix
        :rtype: array[Float, n_rows x n_columns]
        """
        if sparse:
            Q_values = []
            Q_rows = []
            Q_columns = []
        else:
            Q = np.zeros((len(rows), len(columns)), dtype=float)

        # Easier lookup for correct column/row
        column_index = {j: i for i, j in enumerate(columns)}
        row_index = {j: i for i, j in enumerate(rows)}

        # First find all the identity rows in a faster way and add them to
        # a set of rows to ignore later.
        # Using a dictionary for lookup is much faster then a nested loop here.
        identity_rows = []
        if len(column_index) < len(row_index):
            for key, column in column_index.items():
                if key in row_index:
                    row = row_index[key]
                    identity_rows.append(row)
                    if sparse:
                        Q_values.append(1)
                        Q_rows.append(row)
                        Q_columns.append(column)
                    else:
                        Q[row, column] = 1
        else:
            for key, row in row_index.items():
                if key in column_index:
                    column = column_index[key]
                    identity_rows.append(row)
                    if sparse:
                        Q_values.append(1)
                        Q_rows.append(row)
                        Q_columns.append(column)
                    else:
                        Q[row, column] = 1
        identity_rows = set(identity_rows)

        # Go through all rows and add values to dependent columns.
        for row, (parameter_type, metabolite, reaction) in enumerate(rows):
            # We can skip identities rows, since we did those before in bulk.
            if row in identity_rows:
                continue
            # Check all things it depends on.
            dependencies = self.dependencies[parameter_type]
            # If a cooperativity factor is defined, we need to multiply
            # all the stoichiometries of the reaction with this factor to
            # get the effective stoichiometry.
            # Note that this only holds for the apparent enzyme kinetics
            # parameters (kcat / vmax) and not for the thermodynamic
            # parameters (A, keq).
            if parameter_type in Parameters.enzymatic:
                cooperativity = self.cooperativities.get(reaction, 1)
            else:
                cooperativity = 1
            for d_p_type, (value, mult_stoich) in dependencies.items():
                # If mult_stoich is True, it is a reaction parameter
                # dependent on metabolite parameters. Thus we need to retrieve
                # the metabolites via the SBMLModel.
                if mult_stoich:
                    it = self.structure.participating_metabolites(reaction)
                    for dependent_metabolite, s in it:
                        # If we have a cooperativity factor, we need to include it in
                        # the apparent stoichiometry here.
                        s = s * cooperativity
                        if d_p_type in Parameters.specific_to_reaction:
                            column = column_index[(d_p_type, dependent_metabolite, reaction)]
                        else:
                            column = column_index[(d_p_type, dependent_metabolite, NA_IDENTIFIER)]
                        if sparse:
                            # Duplicate entries are summed up in the coo matrix construction.
                            Q_values.append(value * s)
                            Q_rows.append(row)
                            Q_columns.append(column)
                        else:
                            Q[row, column] += value * s
                # Else we only have the single parameter to worry about
                else:
                    column = column_index[(d_p_type, metabolite, reaction)]
                    if sparse:
                        # Duplicate entries are summed up in the coo matrix construction.
                        Q_values.append(value)
                        Q_rows.append(row)
                        Q_columns.append(column)
                    else:
                        Q[row, column] += value

        if sparse:
            # coo is coordinate based and efficient for creating the initial sparse matrix.
            Q = scipy.sparse.coo_matrix((Q_values, (Q_rows, Q_columns)),
                                        shape=(len(rows), len(columns)),
                                        dtype=float)
            if not annotated:
                # csr is a Compressed Sparse Row matrix, allowing more efficient math operations.
                # than coo matrices.
                Q = Q.tocsr()

        if annotated:
            if sparse:
                # This converts to coo before creating the DataFrame, which is why we didn't
                # convert it above.
                return pd.SparseDataFrame(data=Q, columns=columns, index=rows)
            return pd.DataFrame(data=Q, columns=columns, index=rows)

        return Q

        # Alternative implementation idea:
        # 1) build standard matrix in blocks (bw)
        # 2) Resample rows depending on data available (fw)

    def _to_log(self, mu, sigma, mask=None):
        """Convert the log normal distributions (mu, sigma) to log scale.

        This calculates the arithmetic mean and variance of a logarithmic distribution.

        :param mu: mean (in linear scaling)
        :type mu: array[N] float
        :param sd: standard deviation (in linear scaling)
        :type sd: array[N] float
        :param mask: Boolean mask for values that should not be scaled, defaults to None
        :type mask: array[N] bool or None, optional

        :returns: mean, standard deviation - in log scaling
        :rtype: array[N] float, array[N] float
        """
        return to_log_mean(mu, sigma, mask)

    def balance(self, sparse=False):
        """Balance the input data, incorporating the model structure and the priors.

        Note that several large matrices might be created, potentially consuming a large
        amount of memory. For large models, it might help to set sparse=True.

        :returns: Object returning all the balanced parameter distributions.
        :rtype: BalancedParameterData
        """
        # Augment data if required.
        if self.augment:
            self.data = self.augment_data(self.augment_only_missing)

        if sparse:
            inv = scipy.sparse.linalg.inv

            def diag(diagonals):
                """Generate a sparse diagonal in csc format.

                This format is most efficient for the subsequent call to inv we'll use.
                """
                return scipy.sparse.diags(diagonals, offsets=0, format='csc')
        else:
            inv = scipy.linalg.inv
            diag = np.diag

        # Convert data inputs to correct log scale
        mask = self.data.df['parameter_type'].isin(Parameters.non_log).values
        x_in_prime, sigma_x_in = self._to_log(self.data.df['mean'],
                                              self.data.df['sd'], mask)

        # Create priors
        q_prior, sigma_prior = self.build_priors()
        mask = np.array([i[0] in Parameters.non_log for i in self.base_quantities], dtype=bool)
        q_prior, sigma_prior = self._to_log(q_prior, sigma_prior, mask)

        # Create backwards predictor (Q_prime)
        Q_prime = self.backward_dependency_matrix(sparse=sparse)

        # Create forward predictor (Q)
        Q = self.forward_dependency_matrix(sparse=sparse)

        # Calculate inverse of covariance
        # Support the potential side-case of an empty input
        if sigma_x_in.size:
            iC_x = inv(diag(np.square(sigma_x_in)))
        iC_prior = inv(diag(np.square(sigma_prior)))

        # Calculate posterior
        if not sigma_x_in.size:
            # In the no data case we can skip some calculations.
            # Although these won't cost much time in that case, the 0 dimensions don't
            # play nice with scipy's sparse matrices so it's easier to skip it.
            C_post = inv(iC_prior)  # TODO: double inverse seems rather inefficient.
            q_post = C_post.dot(iC_prior.dot(q_prior))
        else:
            temp = Q_prime.T.dot(iC_x)
            C_post = inv(iC_prior + temp.dot(Q_prime))
            q_post = C_post.dot(temp.dot(x_in_prime) +
                                iC_prior.dot(q_prior))

        if sparse:
            # Finally map results back to dense format.
            # q_post should be dense already.
            C_post = C_post.toarray()
            Q = Q.toarray()

        return BalancedParameterData(q_post, C_post, Q, self.all_quantities, self.T, self.R)


if __name__ == "__main__":
    # Make scipy sparse efficiency warnings raise instead of warn for testing.
    import warnings
    warnings.simplefilter('error', scipy.sparse.SparseEfficiencyWarning)

    data_empty = ParameterData([], [], [], [], [])
    priors = {
        Parameters.mu: (-10, 2.5),
        Parameters.kv: (1, .25),
        Parameters.km: (1, .25),
        Parameters.c: (1, .25),
        Parameters.u: (1, .25),
        # Derived parameters
        Parameters.keq: (1, 10),
        Parameters.kcat_prod: (1, 1),
        Parameters.kcat_sub: (1, 1),
        Parameters.vmax: (1, 1),
        Parameters.A: (0, 1),
        Parameters.mu_p: (-10, 10),
    }

    # Test case
    metabolites = ['A', 'B', 'C', 'D', 'E', 'F']
    reactions = ['X', 'Y', 'Z']
    data = [
        (Parameters.c, 'A', None, 1.0, 0.1),
        (Parameters.c, 'B', None, 9.0, 0.001),
        (Parameters.km, 'A', 'X', 1.2, 0.1),
        (Parameters.km, 'A', 'Y', 0.2, 0.1),
        (Parameters.vmax, None, 'X', 1.2, 0.2),
        (Parameters.kcat_prod, None, 'Y', 2.2, 0.2),
        (Parameters.km, 'C', 'Z', 3.2, 0.2),
        (Parameters.keq, None, 'Z', 5.0, 0.001),
        (Parameters.keq, None, 'X', 0.1, 0.1)
    ]
    data = ParameterData(*zip(*data))

    S = np.zeros((len(metabolites), len(reactions)))
    S[:, 0] = [-1, 1, 0, 0, 0, 0]
    S[:, 1] = [1, 0, -1, 1, 0, 0]
    S[:, 2] = [0, 0, 1, 0, -1, 1]
    structure = SBMLModel.from_structure(reactions, metabolites, S)

    # Case with cooperativities
    cooperativities = {'X': 1.5, 'Y': .5}
    test_0 = Balancer(priors, data, structure, T=10, R=2, augment=True,
                      cooperativities=cooperativities)

    # Cases with data
    test_1 = Balancer(priors, data, structure, T=10, R=2, augment=True)
    test_2 = Balancer(priors, data, structure, T=10, R=2, augment=False)

    # Cases with no data
    test_3 = Balancer(priors, data_empty, structure, T=10, R=2, augment=True)
    test_4 = Balancer(priors, data_empty, structure, T=10, R=2, augment=False)

    # First test case with semi realistic values.
    # Priors are linear scale here and will be converted inside.
    priors_r = {
        # Base quantities
        Parameters.mu: (-880.0, 680.00),
        Parameters.kv: (10.0, 6.26),
        Parameters.km: (0.1, 6.26),
        Parameters.c: (0.1, 10.32),
        Parameters.u: (0.0001, 10.32),
        Parameters.ki: (0.1, 6.26),
        Parameters.ka: (0.1, 6.26),
        # Derived quantities
        Parameters.keq: (1.0, 10.32),
        Parameters.kcat_prod: (10.0, 10.32),
        Parameters.kcat_sub: (10.0, 10.32),
        Parameters.vmax: (0.001, 17.01),
        Parameters.A: (0.0, 10.00),
        Parameters.mu_p: (-880.0, 680.00),
    }

    metabolites = ['G6P', 'F6P']
    reactions = ['PGI']
    data = [
        (Parameters.c, 'G6P', None, 10.0, 1.0),
        (Parameters.c, 'F6P', None, 10.0, 1.0),
        (Parameters.km, 'G6P', 'PGI', 0.28, 0.056),
        (Parameters.km, 'F6P', 'PGI', 0.147, 0.0294),
        (Parameters.keq, None, 'PGI', 0.361, 0.0361),
        (Parameters.vmax, None, 'PGI', 1511, 151),
    ]
    S = np.zeros((len(metabolites), len(reactions)))
    S[:, 0] = [-1, 1]

    data_r = ParameterData(*zip(*data))
    structure_r = SBMLModel.from_structure(reactions, metabolites, S)
    test_r = Balancer(priors_r, data_r, structure_r,
                      T=300, R=8.314 / 1000., augment=False)
    test_r_2 = Balancer(priors_r, data_r, structure_r,
                        T=300, R=8.314 / 1000., augment=True)

    # Note that these require SBML files not included in the module.
    # They can be downloaded from the BioModels database if required.
    if False:
        # Mid sized model
        medium_model = 'data/ecoli_core_model_updated.xml'
        ignore = {'R_Biomass_Ecoli_core_w_GAM'}
        structure_m = SBMLModel(medium_model,
                                ignore_exchanges=True,
                                ignore_reactions=ignore)
        test_5 = Balancer(priors, data_empty, structure_m,
                          T=10, R=2, augment=True)
        test_6 = Balancer(priors, data_empty, structure_m,
                          T=10, R=2, augment=False)

        # Some tests for sparse and non-sparse implementation equality.
        for i in (test_1, test_2, test_2, test_4, test_5, test_6, test_r, test_r_2):
            assert np.allclose(i.forward_dependency_matrix(sparse=True).toarray(),
                               i.forward_dependency_matrix(sparse=False))
            assert np.allclose(i.backward_dependency_matrix(sparse=True).toarray(),
                               i.backward_dependency_matrix(sparse=False))
            assert np.allclose(i.balance(sparse=True).to_frame().values,
                               i.balance(sparse=False).to_frame().values)

        # Large model (Warning: This can use a lot of memory to balance!)
        large_model = 'data/iJO1366.xml'
        ignore = {'R_Ec_biomass_iJO1366_WT_53p95M', 'R_Ec_biomass_iJO1366_core_53p95M'}
        structure_l = SBMLModel(large_model,
                                ignore_exchanges=True,
                                ignore_reactions=ignore)
        test_7 = Balancer(priors, data_empty, structure_l,
                          T=10, R=2, augment=False)
        # test_6.balance(sparse=True)
